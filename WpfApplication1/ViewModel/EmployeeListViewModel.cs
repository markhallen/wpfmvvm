﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows.Media;
using WpfApplication1.DataAccess;

namespace WpfApplication1.ViewModel
{
    class EmployeeListViewModel : ViewModelBase
    {
        readonly EmployeeRepository _employeeReposityory;

        RelayCommand _invasionCommand; 

        // AllEmployees will be a property that we can consume in our view
        public ObservableCollection<Model.Employee> AllEmployees
        {
            get;
            private set;
        }

        public EmployeeListViewModel(EmployeeRepository employeeRepository)
        {
            if(employeeRepository==null)
            {
                throw new ArgumentNullException("employeeRepository");
            }
            _employeeReposityory = employeeRepository;
            this.AllEmployees = new ObservableCollection<Model.Employee>(_employeeReposityory.GetEmployees());
        }

        protected override void OnDispose()
        {
            this.AllEmployees.Clear();
        }

        private Brush _bgBrush;
        public Brush BackgroundBrush
        {
            get
            {
                return _bgBrush;
            }
            set
            {
                _bgBrush = value;
                OnPropertyChanged("BackgroundBrush");
            }
        }

        public ICommand InvasionCommand
        {
            get
            {
                if(_invasionCommand == null)
                {
                    _invasionCommand = new RelayCommand(param => this.InvasionCommandExecute(), param => this.InvasionCommandCanExecute);
                }
                return _invasionCommand;
            }
        }

        void InvasionCommandExecute()
        {
            bool isinvasion = true;
            foreach(Model.Employee emp in this.AllEmployees)
            {
                if(emp.LastName.Trim().ToLower()!="smith")
                {
                    isinvasion = false;
                }
            }
            if (isinvasion)
            {
                BackgroundBrush = new SolidColorBrush(Colors.Green);
            }
            else
            {
                BackgroundBrush = new SolidColorBrush(Colors.White);
            }
        }

        bool InvasionCommandCanExecute
        {
            get
            {
                if(this.AllEmployees.Count==0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
