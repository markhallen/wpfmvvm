﻿using System.Collections.Generic;
using WpfApplication1.Model;

namespace WpfApplication1.DataAccess
{
    public class EmployeeRepository
    {
        readonly List<Employee> _employees;

        public EmployeeRepository()
        {
            if(_employees== null)
            {
                _employees = new List<Employee>();
            }
            // statically create the list
            _employees.Add(Employee.CreateEmployee("John", "Smith"));
            _employees.Add(Employee.CreateEmployee("Jill", "Jones"));
            _employees.Add(Employee.CreateEmployee("Cath", "Morts"));
        }

        public List<Employee> GetEmployees()
        {
            // shallow copy of our local employee list
            return new List<Employee>(_employees);
        }
    }
}
